import React from "react";
import {Route, Redirect} from 'react-router-dom'

interface IProps {
  isAuthenticated: boolean
  component: any
  path: string
}

const PublicRoute = ({isAuthenticated, component: Component, ...rest}: any) => {
  return <Route {...rest} render={(props: any) => (
    (!isAuthenticated)
      ?
      <Component {...props}/>
      :
      <Redirect to='/'/>
  )}/>
}

export default PublicRoute;