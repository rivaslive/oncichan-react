import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import AppLayout from "../components/Layout/AppLayout";
import {DC, Marvel, HeroDetails} from "../pages";
import SearchPage from "../pages/Heroes/SearchPage";

const DashboardRoutes = () => {
  return <AppLayout>
    <Switch>
      <Route exact path='/' component={Marvel}/>
      <Route exact path='/dc' component={DC}/>
      <Route exact path='/hero/:heroId' component={HeroDetails}/>
      <Route exact path='/search' component={SearchPage}/>
  
      <Redirect to='/'/>
    </Switch>
  </AppLayout>
}

export default DashboardRoutes;