import React from "react";
import {Route, Redirect} from 'react-router-dom'

interface IProps {
  isAuthenticated: boolean
  component: any
  path: string
}

const PrivateRoute = ({isAuthenticated, component: Component, ...rest}: any) => {
  return <Route {...rest} render={(props: any) => (
    (isAuthenticated)
      ?
      <Component {...props}/>
      :
      <Redirect to='/account/login'/>
  )}/>
}

export default PrivateRoute;