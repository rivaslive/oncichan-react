import React, {useContext} from 'react';
import {
  BrowserRouter as Router,
  Switch,
} from 'react-router-dom'
import {AccountPage} from '../pages'
import DashboardRoutes from "./DashboardRoutes";
import PrivateRoute from "./PrivateRoute";
import AuthContext from "../Auth/AuthContext";
import PublicRoute from './PublicRoute';

const RouterApp = () => {
  const {user} = useContext<any>(AuthContext)
  
  return <Router>
    <Switch>
      <PublicRoute isAuthenticated={user.logged} path='/account/login' component={AccountPage}/>
      <PrivateRoute isAuthenticated={user.logged} path='/' component={DashboardRoutes}/>
    </Switch>
  </Router>
}

export default RouterApp;