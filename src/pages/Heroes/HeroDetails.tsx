import React from 'react';
import {Row, Col, Spin} from 'antd';
import {useParams, Redirect} from 'react-router-dom'
import useFetchHeroById from "../../hooks/useFetchHeroById";
import SpaceDetailHero from './components/SpaceDetailHero';

const HeroDetails = () => {
  const {heroId} = useParams()
  const {hero, loading} = useFetchHeroById(heroId)
  if (!hero) return <Redirect to='/'/>
  const {id, superhero, publisher, alter_ego, first_appearance, characters} = hero
  
  return <>
    {
      loading ?
        <div style={{textAlign: 'center', padding: 50}}><Spin size="large"/></div>
        :
        <Row gutter={16} style={{overflow: 'hidden'}}>
          <Col sm={24} md={9}>
            <img
              className='full-image animate__animated animate__fadeInUp'
              src={`${process.env.PUBLIC_URL}/assets/heroes/${id}.jpg`} alt={superhero}
            />
          </Col>
          
          <Col
            className='animate__animated animate__fadeInDown'
            style={{padding: 20}}
            sm={24}
            md={15}
          >
            <SpaceDetailHero
              superhero={superhero}
              alter_ego={alter_ego}
              characters={characters}
              first_appearance={first_appearance}
              publisher={publisher}
            />
          </Col>
        </Row>
    }
  </>
}

export default HeroDetails;