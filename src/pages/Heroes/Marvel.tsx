import React from 'react';
import HeroList from './components/HeroList';

const Marvel = () => {
  return <>
    <HeroList publisher='Marvel Comics' title='Marvel Comics'/>
  </>
}

export default Marvel;