import React from 'react';
import {Row, Col, Typography} from 'antd'
import {CardHero, SkeletonHeroList} from '../../../components';
import useFetchMockData from "../../../hooks/useFetchMockData";

const HeroList = ({title, publisher}: IProps) => {
  const {heroes, loading} = useFetchMockData(publisher)
  return <>
    <Title level={1}>{title}</Title>
    <Row gutter={[16, 16]} justify='center'>
      {
        loading ?
          <SkeletonHeroList element={20}/>
          :
          heroes.map((hero: IHero) => {
            return <Col key={hero.id}>
              <CardHero {...hero}/>
            </Col>
          })
      }
    </Row>
  </>
}

export const HeroListSearch = ({heroes}: {heroes: IHero[]}) => {
  return <>
    <Row gutter={[16, 16]} justify='center'>
      {
          heroes.map((hero: IHero) => {
            return <Col key={hero.id}>
              <CardHero {...hero}/>
            </Col>
          })
      }
    </Row>
  </>
}

interface IProps {
  title: string
  publisher: "DC Comics" | "Marvel Comics"
}

export interface IHero {
  id: string
  superhero: string
  publisher: string
  alter_ego: string
  first_appearance: string
  characters: string
}

const {Title} = Typography
export default HeroList;