import React from 'react';
import {useHistory} from 'react-router-dom'
import {Button, Space, Typography} from "antd";

const {Text, Title} = Typography


const SpaceDetailHero = ({alter_ego, publisher, characters, first_appearance, superhero}: IProps) => {
  const history = useHistory()
  
  const returnClick = () => {
    history.push(publisher === 'Marvel Comics' ? '/' : '/dc')
  }
  return <>
    <Title>{superhero}</Title>
    <Space direction='vertical'>
      <div>
        <Text className='detail-space-item'>Alter ego: </Text>
        <Text>{alter_ego}</Text>
      </div>
      <div>
        <Text className='detail-space-item'>Publisher: </Text>
        <Text className='font-size-18'>{publisher}</Text>
      </div>
      <div>
        <Text className='detail-space-item'>Characters: </Text>
        <Text className='font-size-18'>{characters}</Text>
      </div>
      <div>
        <Text className='detail-space-item'>First Appearance: </Text>
        <Text className='font-size-18'>{first_appearance}</Text>
      </div>
      <div style={{marginTop: 30}}>
        <Button type='primary' onClick={returnClick}>Return to list</Button>
      </div>
    </Space>
  </>
}


interface IProps {
  alter_ego: string,
  publisher: string,
  characters: string,
  first_appearance: string,
  superhero: string
}

export default SpaceDetailHero;