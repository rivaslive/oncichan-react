import React, {useState, memo, useEffect} from 'react';
import {Typography, Input, Alert} from 'antd'
import queryString from 'query-string'
import useFetchSearch from "../../hooks/useFetchSearch";
import {useLocation, useHistory} from 'react-router-dom';
import {SkeletonHeroList} from "../../components";
import {HeroListSearch} from './components/HeroList';

const {Title} = Typography

const SearchPage = () => {
  const history = useHistory()
  const location = useLocation()
  const {q} = queryString.parse(location.search)
  const Q = q === undefined || q === 'null' || q === '' ? '' : `${q}`
  const [inputValue, setInputValue] = useState(Q)
  const [search, setSearch] = useState(Q)
  const {heroes, loading} = useFetchSearch(search)
  
  useEffect(() => {
    history.push(`/search?q=${search}`)
  }, [search, history])
  
  const handleSearch = ({target: {value}}: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(value)
  }
  
  return <>
    <SearchInput onFinish={setSearch} handleSearch={handleSearch} inputValue={inputValue}/>
    
    <Title level={4}>Results: {heroes.length}</Title>
    
    {
      loading
        ?
        <SkeletonHeroList element={4}/>
        :
        <HeroListSearch heroes={heroes}/>
    }
    <div style={{width: 400, maxWidth: '100%', marginLeft: 'auto', marginRight: 'auto', marginTop: 30}}>
      {
        heroes.length === 0 && !search
          ?
          <Alert className='animate__animated animate__fadeIn' message="Find a hero" type="info" description='Enter a keyword in the search engine' showIcon/>
          :
          heroes.length === 0 && search
          &&
					<Alert className='animate__animated animate__fadeIn' message="Not match search" type="error" description='Searching by name or by publisher may yield better results' showIcon/>
      }
    </div>
  </>
}

const SearchInput = memo(({onFinish, handleSearch, inputValue}: { onFinish: any, handleSearch: any, inputValue: string }) => {
  const finish = (value: string) => {
    onFinish(!value ? null : value)
  }
  
  return <div style={{width: 400, maxWidth: '100%', marginLeft: 'auto', marginRight: 'auto'}}>
    <Input.Search placeholder="Search..." enterButton onSearch={finish} onChange={handleSearch} value={inputValue}/>
  </div>
})

export default SearchPage;