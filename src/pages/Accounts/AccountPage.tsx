import React, {useContext} from 'react';
import {Typography, Form, Input, Button} from 'antd'
import AuthContext from "../../Auth/AuthContext";
import Types from "../../Types/Types";
import { useHistory } from 'react-router-dom';

const {Title, Text} = Typography

const layout = {
  labelCol: {span: 24},
  wrapperCol: {span: 24}
};

const tailLayout = {
  wrapperCol: {span: 24},
};

const AccountPage = () => {
  const history = useHistory()
  const {dispatch} = useContext<any>(AuthContext)
  
  const onFinish = (values: any) => {
    dispatch({type: Types.login, payload: {name: values.username}})
    history.replace('/')
  }
  
  return <div className='bg-login'>
    <div className="login-body">
      <div className='login-body-container'>
        <div className='login-body-content'>
          <Title level={2} style={{color: 'white'}}>Sign in</Title>
          
          <div style={{width: '100%', padding: 0, position: 'relative', marginTop: 30}}>
            <Form
              {...layout}
              name="basic"
              initialValues={{remember: true}}
              onFinish={onFinish}
            >
              <Form.Item
                label={<span style={{color: 'white'}}>Username</span>}
                name="username"
                rules={[{required: true, message: 'Please input your username!'}]}
              >
                <Input size='large' className='input-login'/>
              </Form.Item>
              
              <Form.Item
                label={<span style={{color: 'white'}}>Password</span>}
                name="password"
                rules={[{required: true, message: 'Please input your password!'}]}
              >
                <Input.Password size='large' className='input-login'/>
              </Form.Item>
              
              <Form.Item {...tailLayout} style={{marginTop: 40}}>
                <Button type="primary" htmlType="submit" style={{width: '100%', height: 50}} size='large'>
                  Login
                </Button>
              </Form.Item>
            </Form>
            <Text style={{color: '#d1d1d1'}}>
              This page is protected by Google reCAPTCHA to verify that you are not
              a robot.
            </Text>
          </div>
        </div>
      </div>
    </div>
  </div>
}

export default AccountPage;