import React from 'react';
import {Skeleton} from 'antd'

const CardHeroSkeleton = () => {
  return <div className='card-skeleton'>
    <Skeleton.Input active style={styles.image} size='large'/>
    <div style={{marginLeft: 20}}>
      <Skeleton.Input
        style={styles.InputLarge}
        size='small'
        active
      />
      <Skeleton.Input
        style={styles.InputMedium}
        size='small'
        active
      />
      <Skeleton.Input
        style={styles.InputMedium2}
        size='small'
        active
      />
    </div>
  </div>
}

const styles = {
  image: {width: 100, height: 150, borderRadius: 16},
  InputLarge: {
    width: 130, marginTop: 20, marginLeft: 15, borderRadius: 4
  },
  InputMedium: {width: 160, marginTop: 20, height: 15, borderRadius: 4},
  InputMedium2: {width: 140, marginLeft: 10, marginTop: 20, height: 15, borderRadius: 4}
}
export default CardHeroSkeleton;