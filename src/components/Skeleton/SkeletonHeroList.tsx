import React, {useEffect, useState} from 'react';
import {Row, Col} from "antd";
import {CardHeroSkeleton} from '..';

interface IProps {
  element: number
}

const SkeletonHeroList = ({element}: IProps) => {
  const [arr, setArr] = useState<number[]>([])
  
  useEffect(() => {
    const valueArray = (a: number[]) => {
      if (a.length >= element) return setArr(a)
      valueArray([...a, a.length++])
    }
    valueArray([])
  }, [element])
  
  return <Row gutter={[16, 16]} justify='center'>
    {
      arr.map(item => {
        return <Col key={item}>
          <CardHeroSkeleton/>
        </Col>
      })
    }
  </Row>
}

export default SkeletonHeroList;