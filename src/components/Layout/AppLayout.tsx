import React from "react";
import {Layout} from 'antd';
import AppHeader from "./AppHeader";

const {Content, Footer} = Layout;

interface IProps {
  children: any
}

const AppLayout = ({children}: IProps) => {
  return <Layout>
    <AppHeader/>
    <Content style={{background: 'white', minHeight: 'calc(100vh - 140px)'}}>
      <main style={{marginTop: 20, padding: 20}}>
        {children}
      </main>
    </Content>
    <Footer style={{textAlign: 'center'}}>Onichan ©2020 Created by Kevin Rivas</Footer>
  </Layout>
}

export default AppLayout