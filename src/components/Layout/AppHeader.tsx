import Types from "../../Types/Types";
import {Link, useHistory} from "react-router-dom";
import {Layout, Menu, Typography} from "antd";
import {LogoutOutlined} from '@ant-design/icons'
import AuthContext from "../../Auth/AuthContext";
import React, {useContext, useState} from 'react';
import useGetCurrent from '../../hooks/useGetCurrent';

const {Text} = Typography

const AppHeader = () => {
  const history = useHistory();
  const getCurrent = useGetCurrent()
  const {dispatch} = useContext<any>(AuthContext)
  const [current, setCurrent] = useState(getCurrent)
  
  const handleClick = ({key}: any) => {
    if (key === 'logout') {
      dispatch({type: Types.logout})
      return history.replace('/account/login')
    }
    setCurrent(key)
    history.push(key)
  };
  
  return <>
    <Layout.Header id='header' className="site-layout-sub-header-background">
      <div style={{cursor: 'pointer'}}>
        <Link to='/'>
          <Typography.Title level={3} className='logo text-center'>Onichan</Typography.Title>
        </Link>
      </div>
      <div style={{float: 'right', cursor: 'pointer'}} title="logout">
        <LogoutOutlined/>
      </div>
    </Layout.Header>
    <Menu mode="horizontal" selectedKeys={[current]} onClick={handleClick}>
      <Menu.Item key="/"><Text title="marvel">Marvel</Text></Menu.Item>
      <Menu.Item key="/dc"><Text title="DC">DC</Text></Menu.Item>
      <Menu.Item key="/search"><Text title="search">Search</Text></Menu.Item>
    </Menu>
  </>
}

export default AppHeader;
