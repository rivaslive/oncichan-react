import React from 'react';
import {Card, Typography} from 'antd';
import {Link, useHistory} from "react-router-dom";
import {IHero} from "../../pages/Heroes/components/HeroList";

const {Text} = Typography

const CardHero = ({first_appearance, characters, alter_ego, publisher, id, superhero}: IHero) => {
  const history = useHistory()
  return <Card
    onClick={() => history.push(`/hero/${id}`)}
    className='card'
    hoverable
  >
    <div className="card-image">
      <img src={`${process.env.PUBLIC_URL}/assets/heroes/${id}.jpg`} alt={superhero}/>
    </div>
    <div className='card-body'>
      <div className="header">
        <Text style={{fontWeight: 700}}>{superhero}</Text>
      </div>
      <div className="body">
        <div style={{marginTop: 5, maxHeight: 20, overflow: 'hidden'}}>
          <Text style={{fontWeight: 400}}>{alter_ego}</Text>
        </div>
        {
          characters !== alter_ego && <div style={{marginTop: 5, maxHeight: 20, overflow: 'hidden'}}>
	          <Text style={{fontWeight: 400}}>{characters}</Text>
          </div>
        }
        <div style={{marginTop: 5, maxHeight: 35, overflow: 'hidden', lineHeight: '17px'}}>
          <Text style={{fontWeight: 400}}><strong>First appearance: </strong>{first_appearance}</Text>
        </div>
        <div style={{textAlign: 'center', padding: 5}}>
          <Link to={`/hero/${id}`}>See more!</Link>
        </div>
      </div>
    </div>
  </Card>
}

export default CardHero;