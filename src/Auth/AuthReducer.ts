import Types from "../Types/Types";

const AuthReducer = (state: any = {}, {type, payload}: { type: string, payload: any }) => {
  switch (type) {
    case Types.login:
      return {
        ...payload,
        logged: true
      }
    
    case Types.logout:
      return {
        logged: false
      }
    
    default:
      return state
  }
}

export default AuthReducer