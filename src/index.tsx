import './index.scss';
import 'antd/dist/antd.css';
import Spin from 'antd/lib/spin'
import ReactDOM from 'react-dom';
import React, {Suspense} from 'react';

const App = React.lazy(() => import('./App'))

const Spinner = () => {
  return <div style={{height: '100vh', width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
    <Spin size={"large"} style={{fontSize: 20}} tip='loading'/>
  </div>
}

ReactDOM.render(
    <Suspense fallback={<Spinner/>}>
      <App/>
    </Suspense>,
  document.getElementById('root')
);