import {HEROES} from "../mock/data";
import {useCallback, useEffect, useRef, useState} from "react";
import {IHero} from "../pages/Heroes/components/HeroList";

const useFetchSearch = (value: string) => {
  const isMounted = useRef(true)
  const [heroes, setHeroes] = useState<IHero[]>([])
  const [loading, setLoading] = useState(true)
  
  useEffect(() => {
    return () => {
      isMounted.current = false
    }
  }, [])
  
  const find = useCallback(() => {
    if (value === '') return setLoading(false)
    if (value === null) {
      setHeroes([])
      setLoading(false)
    }
    const find = HEROES.filter(hero =>
      hero.superhero.toLocaleLowerCase().includes(value) ||
      hero.alter_ego.toLocaleLowerCase().includes(value) ||
      hero.publisher.toLocaleLowerCase().includes(value) ||
      hero.characters.toLocaleLowerCase().includes(value)
    )
    setHeroes(find)
    setLoading(false)
  }, [value])
  
  useEffect(() => {
    find()
  }, [find])
  
  return {heroes, loading}
}

export default useFetchSearch;