import {HEROES} from "../mock/data"
import {useEffect, useState} from "react";

const useFetchMockData = (publisher: 'DC Comics' | 'Marvel Comics' = 'Marvel Comics') => {
  const [heroes, setHeroes] = useState<any>([])
  const [loading, setLoading] = useState<boolean>(true)
  
  useEffect(() => {
    setTimeout(() => {
      setHeroes(HEROES.filter(item => item.publisher === publisher))
      setLoading(false)
    }, 200)
    clearTimeout()
  }, [publisher])
  
  return {heroes, loading}
}

export default useFetchMockData