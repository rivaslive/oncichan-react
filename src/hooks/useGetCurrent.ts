import { useLocation } from "react-router-dom";
import {useEffect, useState} from "react";

const useGetCurrent = () => {
  const {pathname} = useLocation()
  const [current, setCurrent] = useState(pathname)
  
  useEffect(() => {
    if (pathname === current) return
    setCurrent(pathname)
  }, [pathname, current])
  
  return current
}

export default useGetCurrent;