import {HEROES} from "../mock/data"
import {useEffect, useState} from "react";

const useFetchHeroById = (id: string) => {
  const [hero, setHero] = useState<any>({})
  const [loading, setLoading] = useState<boolean>(true)
  
  useEffect(() => {
    setTimeout(() => {
      setHero(HEROES.find(item => item.id === id))
      setLoading(false)
    }, 1000)
    clearTimeout()
  }, [id])
  
  return {hero, loading}
}

export default useFetchHeroById