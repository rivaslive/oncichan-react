import {ConfigProvider} from "antd";
import esES from "antd/es/locale/es_ES";
import RouterApp from './routers/AppRouter';
import AuthContext from './Auth/AuthContext';
import AuthReducer from "./Auth/AuthReducer";
import React, {useEffect, useReducer} from 'react';

const App = () => {
  const [user, dispatch] = useReducer(AuthReducer, {}, init)
  
  useEffect(() => {
    localStorage.setItem('auth', JSON.stringify({...user}))
  }, [user])
  
  return <ConfigProvider locale={esES}>
    <AuthContext.Provider value={{user, dispatch}}>
      <RouterApp/>
    </AuthContext.Provider>
  </ConfigProvider>
}

const init = () => {
  return JSON.parse(localStorage.getItem('auth') as string) || {logged: false}
}

export default App;